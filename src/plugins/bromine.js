import Vue from 'vue'

import '@/components/_styles/index.js'

import View from '@/components/View'
import Plot from '@/components/Plot'

Vue.component(View.name, View)
Vue.component(Plot.name, Plot)
Vue.component(Plot.Grid.name, Plot.Grid)
