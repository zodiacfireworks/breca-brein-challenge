import Vue from 'vue'
import { Button, Icon, Row, Col, Form, Input, InputNumber, Divider, Select } from 'ant-design-vue'

Vue.component(Row.name, Row)
Vue.component(Col.name, Col)

Vue.component(Icon.name, Icon)
Vue.component(Button.name, Button)
Vue.component(Form.name, Form)
Vue.component(Form.Item.name, Form.Item)
Vue.component(Input.name, Input)
Vue.component(Input.Group.name, Input.Group)
Vue.component(Input.TextArea.name, Input.TextArea)
Vue.component(InputNumber.name, InputNumber)
Vue.component(Divider.name, Divider)
Vue.component(Select.name, Select)
Vue.component(Select.Option.name, Select.Option)
