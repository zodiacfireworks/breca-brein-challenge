import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue')
  },
  {
    path: '/settings',
    name: 'settings',
    component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
    meta: {
      showDashboardDrawer: true
    }
  },
  {
    path: '/plot/add',
    name: 'plot-add',
    component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
    meta: {
      addPlot: true
    }
  },
  {
    path: '/plot/:id/edit',
    name: 'plot-edit',
    component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
    meta: {
      editPlot: true
    }
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
