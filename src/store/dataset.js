import { SAMPLE_DATASET_LIST } from '@/_utils/sampleData'

const initialState = {
  objectList: SAMPLE_DATASET_LIST
}

const getters = {
  getObject: (state) => (id) => {
    return state.objectList.find(object => object.id === id)
  }
}

const actions = {}

const mutations = {}

export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
}
