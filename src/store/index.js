import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import createLogger from 'vuex/dist/logger'

import plot from './plot'
import dashboard from './dashboard'
import dataset from './dataset'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  modules: {
    plot,
    dashboard,
    dataset
  },
  strict: debug,
  plugins: [vuexLocal.plugin, ...(debug ? [createLogger()] : [])]
})
