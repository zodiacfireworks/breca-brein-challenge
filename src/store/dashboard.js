import { SAMPLE_DASHBOARD } from '@/_utils/sampleData'

const initialState = SAMPLE_DASHBOARD

const actions = {
  setTitle (
    { commit },
    {
      title
    } = {}) {
    commit('SET_TITLE', title)
  }
}

const mutations = {
  SET_TITLE (state, title) {
    state.title = title
  }
}

export default {
  namespaced: true,
  state: initialState,
  actions,
  mutations
}
