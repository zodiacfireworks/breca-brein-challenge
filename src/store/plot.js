import { SAMPLE_PLOT_LIST } from '@/_utils/sampleData'

const initialState = {
  objectList: SAMPLE_PLOT_LIST
}

const getters = {
  getObject: (state) => (id) => {
    return state.objectList.find(object => object.id === id)
  }
}
const actions = {
  saveObject (
    {
      commit,
      state
    },
    {
      plot
    } = {}) {
    const index = state.objectList.findIndex(obj => obj.id === plot.id)
    if (index >= 0) {
      commit('UPDATE_PLOT', { index, plot })
    } else {
      commit('CREATE_PLOT', plot)
    }
  }
}

const mutations = {
  UPDATE_PLOT (state, { index, plot }) {
    const objectList = [...state.objectList]
    objectList[index] = { ...plot }
    state.objectList = [...objectList]
  },
  CREATE_PLOT (state, plot) {
    const objectList = [...state.objectList]
    objectList.push(plot)
    state.objectList = [...objectList]
  }
}

export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
}
