import { BILLBOARD, HISTOGRAM, TIMESERIES } from './_types'

export const getPlotProcess = (settings) => {
  if (settings.type === BILLBOARD) {
    return settings
  }
  if (settings.type === HISTOGRAM) {
    return settings
  }
  if (settings.type === TIMESERIES) {
    return settings
  }
  return null
}

export const process = (settings) => {
  return getPlotProcess(settings)
}
