export const BILLBOARD = 'billboard'
export const HISTOGRAM = 'histogram'
export const TIMESERIES = 'timeseries'

export const TYPES = [
  {
    id: BILLBOARD,
    label: 'Letrero'
  },
  {
    id: HISTOGRAM,
    label: 'Histograma'
  },
  {
    id: TIMESERIES,
    label: 'Serie de tiempo'
  }
]

export default TYPES
