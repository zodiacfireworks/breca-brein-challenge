const rxIsInt = /^\d+$/
const rxIsFloat = /^\d*\.\d+$|^\d+\.\d*$/
/**
 * If a string has leading or trailing space,
 * contains a comma double quote or a newline
 * it needs to be quoted in CSV output
 */
const rxNeedsQuoting = /^\s|\s$|,|"|\n/
const trim = (s) => {
  // Fix 3.1 has a native trim function, it's about 10x faster, use it if it exists
  if (String.prototype.trim) {
    return s.trim()
  } else {
    return s.replace(/^\s*/, '').replace(/\s*$/, '')
  }
}

const chomp = (s, lineterminator) => {
  if (s.charAt(s.length - lineterminator.length) !== lineterminator) {
    // Does not end with \n, just return string
    return s
  } else {
    // Remove the \n
    return s.substring(0, s.length - lineterminator.length)
  }
}

const normalizeLineTerminator = (csvString, dialect) => {
  dialect = dialect || {}

  // Try to guess line terminator if it's not provided.
  if (!dialect.lineterminator) {
    return csvString.replace(/(\r\n|\n|\r)/gm, '\n')
  }
  // if not return the string untouched.
  return csvString
}

const objectToArray = (dataToSerialize) => {
  const array = []
  const fieldNames = []
  for (let ii = 0; ii < dataToSerialize.fields.length; ii++) {
    fieldNames.push(dataToSerialize.fields[ii].id)
  }
  array.push(fieldNames)
  for (let ii = 0; ii < dataToSerialize.records.length; ii++) {
    const tmp = []
    const record = dataToSerialize.records[ii]
    for (let jj = 0; jj < fieldNames.length; jj++) {
      tmp.push(record[fieldNames[jj]])
    }
    array.push(tmp)
  }
  return array
}

const normalizeDialectOptions = (options) => {
  // note lower case compared to CSV DDF
  const out = {
    delimiter: ',',
    doublequote: true,
    lineterminator: '\n',
    quotechar: '"',
    skipinitialspace: true,
    skipinitialrows: 0
  }
  for (const key in options) {
    if (key === 'trim') {
      out.skipinitialspace = options.trim
    } else {
      out[key.toLowerCase()] = options[key]
    }
  }
  return out
}

/**
 * parse
 *
 * For docs see the README
 *
 * Heavily based on uselesscode's JS CSV parser (MIT Licensed):
 * http://www.uselesscode.org/javascript/csv/
 */
export const parse = (s, dialect) => {
  // When line terminator is not provided then we try to guess it
  // and normalize it across the file.
  if (!dialect || (dialect && !dialect.lineterminator)) {
    s = normalizeLineTerminator(s, dialect)
  }

  // Get rid of any trailing \n
  const options = normalizeDialectOptions(dialect)
  s = chomp(s, options.lineterminator)

  let cur = '' // The character we are currently processing.
  let inQuote = false
  let fieldQuoted = false
  let field = '' // Buffer for building up the current field
  let row = []
  let out = []
  let i

  const processField = (field) => {
    if (fieldQuoted !== true) {
      // If field is empty set to null
      if (field === '') {
        field = null
        // If the field was not quoted and we are trimming fields, trim it
      } else if (options.skipinitialspace === true) {
        field = trim(field)
      }

      // Convert unquoted numbers to their appropriate types
      if (rxIsInt.test(field)) {
        field = parseInt(field, 10)
      } else if (rxIsFloat.test(field)) {
        field = parseFloat(field, 10)
      }
    }
    return field
  }

  for (i = 0; i < s.length; i += 1) {
    cur = s.charAt(i)

    // If we are at a EOF or EOR
    if (
      inQuote === false &&
      (cur === options.delimiter || cur === options.lineterminator)
    ) {
      field = processField(field)
      // Add the current field to the current row
      row.push(field)
      // If this is EOR append row to output and flush row
      if (cur === options.lineterminator) {
        out.push(row)
        row = []
      }
      // Flush the field buffer
      field = ''
      fieldQuoted = false
    } else {
      // If it's not a quotechar, add it to the field buffer
      if (cur !== options.quotechar) {
        field += cur
      } else {
        if (!inQuote) {
          // We are not in a quote, start a quote
          inQuote = true
          fieldQuoted = true
        } else {
          // Next char is quotechar, this is an escaped quotechar
          if (s.charAt(i + 1) === options.quotechar) {
            field += options.quotechar
            // Skip the next char
            i += 1
          } else {
            // It's not escaping, so end quote
            inQuote = false
          }
        }
      }
    }
  }

  // Add the last field
  field = processField(field)
  row.push(field)
  out.push(row)

  // Expose the ability to discard initial rows
  if (options.skipinitialrows) out = out.slice(options.skipinitialrows)

  return out
}

/*
 * serialize
 *
 * See README for docs
 *
 * Heavily based on uselesscode's JS CSV serializer (MIT Licensed):
 * http://www.uselesscode.org/javascript/csv/
 */
export const serialize = (dataToSerialize, dialect) => {
  let a = null
  if (dataToSerialize instanceof Array) {
    a = dataToSerialize
  } else {
    a = objectToArray(dataToSerialize)
  }
  const options = normalizeDialectOptions(dialect)

  let cur = '' // The character we are currently processing.
  let field = '' // Buffer for building up the current field
  let row = ''
  let out = ''
  let i
  let j

  const processField = (field) => {
    if (field === null) {
      // If field is null set to empty string
      field = ''
    } else if (typeof field === 'string' && rxNeedsQuoting.test(field)) {
      if (options.doublequote) {
        field = field.replace(/"/g, '""')
      }
      // Convert string to delimited string
      field = options.quotechar + field + options.quotechar
    } else if (typeof field === 'number') {
      // Convert number to string
      field = field.toString(10)
    }

    return field
  }

  for (i = 0; i < a.length; i += 1) {
    cur = a[i]

    for (j = 0; j < cur.length; j += 1) {
      field = processField(cur[j])
      // If this is EOR append row to output and flush row
      if (j === cur.length - 1) {
        row += field
        out += row + options.lineterminator
        row = ''
      } else {
        // Add the current field to the current row
        row += field + options.delimiter
      }
      // Flush the field buffer
      field = ''
    }
  }

  return out
}

// Convert array of rows in { records: [ ...] , fields: [ ... ] }
// @param {Boolean} noHeaderRow If true assume that first row is not a header (i.e. list of fields but is data.
export const extractFields = (rows, noFields) => {
  if (noFields.noHeaderRow !== true && rows.length > 0) {
    return {
      fields: rows[0],
      records: rows.slice(1)
    }
  } else {
    return {
      records: rows
    }
  }
}

export const csvDataset = (s) => {
  return extractFields(parse(s), {})
}

export const jsonDataset = (s) => {
  const { fields, records } = extractFields(parse(s), {})
  return {
    fields,
    records: records.map(
      row => row.map(
        (item, index) => [fields[index], item]
      ).reduce(
        (obj, item) => ({ ...obj, [item[0]]: item[1] }),
        {}
      )
    )
  }
}
