export const AREAS = {
  m2: {
    type: Number
  },
  floor_num: {
    type: String
  },
  tenant_id: {
    type: String
  },
  venue_id: {
    type: String
  },
  id: {
    type: String
  },
  group_id: {
    type: String
  }
}

export const COUNTS = {
  date: {
    type: Date
  },
  num_ingress: {
    type: Number
  },
  num_transit: {
    type: Number
  },
  area_id: {
    type: String
  }
}

export const LIKES = {
  clean_name: {
    type: String
  },
  email: {
    type: String
  }
}

export const PAGES = {
  clean_name: {
    type: String
  },
  brand: {
    type: String
  }
}

export const SALES = {
  ss_tenant_name: {
    type: String
  },
  date: {
    type: Date
  },
  num_sales: {
    type: Number
  },
  num_transactions: {
    type: Number
  },
  venue_id: {
    type: String
  },
  ss_tenant_id: {
    type: String
  }
}

export const SS_TENANTS = {
  ss_tenant_id: {
    type: String
  },
  tenant_id: {
    type: String
  },
  area_id: {
    type: String
  },
  venue_id: {
    type: String
  }
}

export const TENANTS = {
  id: {
    type: String
  },
  brand: {
    type: String
  },
  category: {
    type: String
  }
}

export const VENUES = {
  id: {
    type: String
  },
  name: {
    type: String
  },
  cluster: {
    type: String
  }
}

export const VISITORS = {
  email: {
    type: String
  },
  gender: {
    type: String
  },
  date_of_birth: {
    type: Date
  }
}

export const VISITS = {
  date: {
    type: Date
  },
  email: {
    type: String
  },
  venue_id: {
    type: String
  }
}

export const SCHEMAS = {
  areas: AREAS,
  counts: COUNTS,
  likes: LIKES,
  pages: PAGES,
  sales: SALES,
  ss_tenants: SS_TENANTS,
  tenants: TENANTS,
  venues: VENUES,
  visitors: VISITORS,
  visits: VISITS
}
