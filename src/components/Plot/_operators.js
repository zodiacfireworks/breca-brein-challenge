export const countUnique = (iterable) => {
  return new Set(iterable).size
}

export const count = (iterable) => {
  return iterable.length
}

export const OPERATORS = {
  countUnique: countUnique,
  count: count
}

export default OPERATORS
