import Plot from './Plot.vue'
import Grid from './Grid.vue'
import Edit from './Edit.vue'
import Add from './Add.vue'

Plot.Grid = Grid
Plot.Edit = Edit
Plot.Add = Add

export default Plot
