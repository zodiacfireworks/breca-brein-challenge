import { uuid4 } from './uuid'

export const SAMPLE_DASHBOARD = {
  defaultTitle: 'Bromine Dashboard',
  title: 'Bromine Dashboard',
  datasets: {
    areas: '/dataset/areas.csv',
    counts: '/dataset/counts.csv',
    likes: '/dataset/likes.csv',
    pages: '/dataset/pages.csv',
    sales: '/dataset/sales.csv',
    ss_tenants: '/dataset/ss_tenants.csv',
    tenants: '/dataset/tenants.csv',
    venues: '/dataset/venues.csv',
    visitors: '/dataset/visitors.csv',
    visits: '/dataset/visits.csv'
  }
}

export const SAMPLE_DATASET_LIST = [
  {
    id: 'areas',
    name: 'Areas',
    url: '/dataset/areas.csv',
    headers: [
      'm2', 'floor_num', 'tenant_id', 'venue_id', 'id', 'group_id'
    ],
    data: null,
    downloading: false
  },
  {
    id: 'counts',
    name: 'Counts',
    url: '/dataset/counts.csv',
    headers: [
      'date', 'num_ingress', 'num_transit', 'area_id'
    ],
    data: null,
    downloading: false
  },
  {
    id: 'likes',
    name: 'Likes',
    url: '/dataset/likes.csv',
    headers: [
      'clean_name', 'email'
    ],
    data: null,
    downloading: false
  },
  {
    id: 'pages',
    name: 'Pages',
    url: '/dataset/pages.csv',
    headers: [
      'clean_name', 'brand'
    ],
    data: null,
    downloading: false
  },
  {
    id: 'sales',
    name: 'Sales',
    url: '/dataset/sales.csv',
    headers: [
      'ss_tenant_name', 'date', 'num_sales', 'num_transactions', 'venue_id', 'ss_tenant_id'
    ],
    data: null,
    downloading: false
  },
  {
    id: 'ss_tenants',
    name: 'SS Tenants',
    url: '/dataset/ss_tenants.csv',
    headers: [
      'ss_tenant_id', 'tenant_id', 'area_id', 'venue_id'
    ],
    data: null,
    downloading: false
  },
  {
    id: 'tenants',
    name: 'Tenants',
    url: '/dataset/tenants.csv',
    headers: [
      'id', 'brand', 'category'
    ],
    data: null,
    downloading: false
  },
  {
    id: 'venues',
    name: 'Venues',
    url: '/dataset/venues.csv',
    headers: [
      'id', 'name', 'cluster'
    ],
    data: null,
    downloading: false
  },
  {
    id: 'visitors',
    name: 'Visitors',
    url: '/dataset/visitors.csv',
    headers: [
      'email', 'gender', 'date_of_birth'
    ],
    data: null,
    downloading: false
  },
  {
    id: 'visits',
    name: 'Visits',
    url: '/dataset/visits.csv',
    headers: [
      'date', 'email', 'venue_id'
    ],
    data: null,
    downloading: false
  }
]

export const SAMPLE_PLOT_LIST = [
  {
    id: uuid4(),
    meta: {
      title: 'Sample Plot 1',
      description: 'Sample description.'
    },
    layout: {
      xs: 24,
      sm: 24,
      md: 12,
      lg: 12,
      xl: 12,
      xxl: 12
    },
    graph: {
      type: null,
      x: {
        dataset: 'areas',
        attribute: '*'
      },
      y: {
        dataset: 'areas',
        attribute: '*'
      },
      link: {
        xAttribute: null,
        yAttribute: null
      }
    }
  },
  {
    id: uuid4(),
    meta: {
      title: 'Sample Plot 2',
      description: 'Sample description.'
    },
    layout: {
      xs: 24,
      sm: 24,
      md: 12,
      lg: 12,
      xl: 12,
      xxl: 12
    },
    graph: {
      type: null,
      x: {
        dataset: 'areas',
        attribute: '*'
      },
      y: {
        dataset: 'areas',
        attribute: '*'
      },
      link: {
        xAttribute: null,
        yAttribute: null
      }
    }
  },
  {
    id: uuid4(),
    meta: {
      title: 'Sample Plot 3',
      description: 'Sample description.'
    },
    layout: {
      xs: 24,
      sm: 24,
      md: 12,
      lg: 12,
      xl: 12,
      xxl: 12
    },
    graph: {
      type: null,
      x: {
        dataset: 'areas',
        attribute: '*'
      },
      y: {
        dataset: 'areas',
        attribute: '*'
      },
      link: {
        xAttribute: null,
        yAttribute: null
      }
    }
  },
  {
    id: uuid4(),
    meta: {
      title: 'Sample Plot 4',
      description: 'Sample description.'
    },
    layout: {
      xs: 24,
      sm: 24,
      md: 12,
      lg: 12,
      xl: 12,
      xxl: 12
    },
    graph: {
      type: null,
      x: {
        dataset: 'areas',
        attribute: '*'
      },
      y: {
        dataset: 'areas',
        attribute: '*'
      },
      link: {
        xAttribute: null,
        yAttribute: null
      }
    }
  }
]
