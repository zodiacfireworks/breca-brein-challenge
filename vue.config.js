const path = require('path')

const theme = {
  'primary-color': '#ff6f00',
  'link-color': '#ff6f00',
  'border-radius-base': '4px',
  'layout-body-background': '#fafafa',
  'layout-header-background': '#fafafa',
  'layout-footer-background': '#fafafa',
  'font-family': '"Fira Sans", "Roboto", sans-serif',
  'code-family': '"Fira Code", "Roboto Mono", monospace',
  'font-size-base': '16px'
}

module.exports = {
  css: {
    extract: true,
    loaderOptions: {
      less: {
        modifyVars: theme,
        javascriptEnabled: true
      }
    }
  },
  configureWebpack: {
    resolve: {
      extensions: ['.less'],
      modules: [path.resolve(__dirname, 'src')]
    }
  }

}
